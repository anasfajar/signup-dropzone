<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Session\Session;


class SigninController extends Controller
{
    public function index(){
        return view('signin');
    }

    public function signinProses(Request $request)
    {
        $credentials = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        if(Auth::attempt($credentials))
        {
            $request->session()->regenerate();
            return redirect()->intended('/daftarPengguna');
        }

        return back()->with('loginError', 'Login Gagal');
    }

    public function signout(Request $request)
    {
        $request->session()->flush();
        
        Auth::logout();

        return redirect('/signin');
    }
}
