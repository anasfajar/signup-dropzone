<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Contracts\Session\Session;

class SignupController extends Controller
{
    public function index(){
        return view('signup');
    }

    public function signupProses(Request $request)
    {
        if ($request->session()->missing('imageName')) {
            return redirect('/signup')->with('error', 'Pastikan Gambar telah di upload');
        }
        $validatedData = $request->validate([
            'namalengkap' => 'required',
            'nik' => 'required|digits:16|numeric',
            'username' => 'required|unique:users,username',
            'password' => 'required',
            'repeatpass' => 'required|same:password'
        ]);

        $validatedData['password'] = Hash::make($validatedData['password']);
        $validatedData['gambar_ktp'] = $request->session()->get('imageName');
        User::create($validatedData);
        $request->session()->forget('imageName');
        return redirect('/signin')->with('success', 'Sign Up Berhasil Silahkan Login');

    }

    public function dropzoneStore(Request $request)
    {
        $image = $request->file('file');
        $imageName = time().'.'.$image->extension();
        $image->move(public_path('images'),$imageName);
        Session(['imageName'=> $imageName]);
        return response()->json(['success'=>$imageName]);
    }
}

