<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Register</title>

    <!-- Custom fonts for this template-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.min.css" integrity="sha512-3g+prZHHfmnvE1HBLwUnVuunaPOob7dpksI7/v6UnF/rnKGwHf/GdEq9K7iEN7qTtW+S0iivTcGpeTBqqB04wA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <!-- <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
                    <div class="col-lg-12">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                                @if(session()->has('error'))<h5 class="h5 text-red-400 mb-2">{{session('error')}}</h5>@endif
                            </div>
                            <form class="user" action="/signup" method="post">
                                @csrf
                                <div class="form-group">
                                    <input type="text" class="form-control form-control-user @error('namalengkap') is-invalid @enderror" id="namalengkap" placeholder="Nama Lengkap" name="namalengkap" value="{{ old('namalengkap')}}" required>
                                    @error('namalengkap')<div class="invalid-feedback">{{$message}}</div>@enderror
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user @error('nik') is-invalid @enderror" id="nik" placeholder="NIK" name="nik" value="{{ old('nik')}}" required>
                                        @error('nik')<div class="invalid-feedback">{{$message}}</div>@enderror
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user @error('username') is-invalid @enderror" id="username" placeholder="Username" name="username" value="{{ old('username')}}" required>
                                        @error('username')<div class="invalid-feedback">{{$message}}</div>@enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="password" class="form-control form-control-user @error('password') is-invalid @enderror" id="exampleInputPassword" placeholder="Password" name="password" required>
                                        @error('password')<div class="invalid-feedback">{{$message}}</div>@enderror
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="password" class="form-control form-control-user @error('repeatpass') is-invalid @enderror" id="exampleRepeatPassword" placeholder="Repeat Password" name="repeatpass" required>
                                        @error('repeatpass')<div class="invalid-feedback">{{$message}}</div>@enderror
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-primary btn-user btn-block d-none" id="submit">
                                </form>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-header">
                                                Upload Gambar KTP
                                            </div>
                                            <div class="card-body">
                                                <form method="post" action="{{route('dz.store')}}" class="dropzone dz-clikable" id="image-upload">
                                                    @csrf
                                                    <div>
                                                        <h3 class="text-center">
                                                            
                                                        </h3>
                                                    </div>
                                                    <div class="dz-default dz-message">
                                                        <span>Klik Untuk Upload</span>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="button" class="btn btn-primary btn-user btn-block" value="Register Account" id="btnSubmit">
                            <!-- </form> -->
                            <hr>
                            <div class="text-center">
                                <a class="small" href="/signin">Already have an account? Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.min.js" integrity="sha512-8l10HpXwk93V4i9Sm38Y1F3H4KJlarwdLndY9S5v+hSAODWMx3QcAVECA23NTMKPtDOi53VFfhIuSsBjjfNGnA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $('#btnSubmit').on('click',function(){
            $('#submit').click();
        });
    </script>

</body>

</html>