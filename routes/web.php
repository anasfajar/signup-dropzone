<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SigninController;
use App\Http\Controllers\SignupController;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [SigninController::class, 'index']);
    Route::get('/daftarPengguna', [DashboardController::class, 'daftarPengguna']);
    Route::get('/signout', [SigninController::class, 'signout']);
Route::get('/signin', [SigninController::class, 'index']);
Route::post('/signin', [SigninController::class, 'signinProses']);
Route::get('/signup', [SignupController::class, 'index']);
Route::post('/signup', [SignupController::class, 'signupProses']);
Route::post('/dzStore', [SignupController::class, 'dropzoneStore'])->name('dz.store');
