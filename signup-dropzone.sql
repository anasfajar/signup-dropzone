-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.20-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for signup-dropzone
CREATE DATABASE IF NOT EXISTS `signup-dropzone` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `signup-dropzone`;

-- Dumping structure for table signup-dropzone.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namalengkap` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nik` varchar(255) DEFAULT NULL,
  `gambar_ktp` varchar(255) DEFAULT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table signup-dropzone.users: ~10 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `namalengkap`, `username`, `password`, `nik`, `gambar_ktp`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'asdasdfasdfasdfasdfasdf', 'qweqwewe', '$2y$10$tp58RLXDESo4pU8vVj5klOJo4iHDhtKRHkwZcLJ7urGP7vMCHhZwi', '1689898989898989', NULL, NULL, '2021-11-18 18:29:10', '2021-11-18 18:29:10'),
	(2, 'anas fajar', 'admintest', '$2y$10$8JjbOz1uhTnDfE6XFBxUVeyikXFFd9CqJ8A.4ByRLXQkudipOm3yq', '1122233322211111', NULL, NULL, '2021-11-18 18:38:58', '2021-11-18 18:38:58'),
	(3, 'anasdfafsdf', '12311111', '$2y$10$7yOTSfobaCW9qraJJTTv5ePDejUuM3AdIou0i7AWOObwlHxOq661m', '1234123412341234', NULL, NULL, '2021-11-18 18:40:26', '2021-11-18 18:40:26'),
	(4, '11111111', '12344321', '$2y$10$ZbgVq4HQYX4vXKUzNRG8QO7bGiUMiMj7KbdoR9qiKYsyJDPkbpCX.', '1234432112344321', NULL, NULL, '2021-11-18 18:41:53', '2021-11-18 18:41:53'),
	(5, 'yesahnsaksj', '13576666666', '$2y$10$ypp0B0CIfuYniYM1kwKeUunryYUORQDst9pEPyleYkSqjwsdO4gDC', '1234567887654321', NULL, NULL, '2021-11-18 18:47:04', '2021-11-18 18:47:04'),
	(6, 'nbjnjnjnjnjnjn', '111222111', '$2y$10$vgJsRjURT5GMrKKGaSp60u2x8NRUC0oMLjTcto0mnsu04DV907oV.', '1112221112221111', NULL, NULL, '2021-11-18 18:48:04', '2021-11-18 18:48:04'),
	(7, 'qweasdxz', 'admintest1', '$2y$10$rZxT5jifCNwMvIasZJUr2ejEBWh7AwFmgYg3Zqq.f1K/FYzwgEoa2', '1111111111111112', NULL, NULL, '2021-11-18 19:16:14', '2021-11-18 19:16:14'),
	(8, 'anasfajar', 'anasfajar', '$2y$10$kYUHYv4VpiIFCj5e6XaFq.yikdXZ6VSOJRJld6TPzAYMAl2xZNr.y', '1111222233334444', '1637288657.jpg', NULL, '2021-11-19 02:24:20', '2021-11-19 02:24:20'),
	(9, 'fajarpraamat', 'anasdasdas', '$2y$10$gr9bfWPMwK1XBHV59RhebOeGwuvs.GXG/5Cm3o7ss6orxvoAO4RY6', '1111222233334555', '1637288657.jpg', NULL, '2021-11-19 02:30:13', '2021-11-19 02:30:13'),
	(10, 'dasdfsadfadsfadf', '91919222222', '$2y$10$UfWTZdb2gECuhIOi4NWODOWTwHMjcBzvgvaq5zI0zEYKAv.HXQLLu', '9109109109109109', '1637288657.jpg', NULL, '2021-11-19 02:32:42', '2021-11-19 02:32:42');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
